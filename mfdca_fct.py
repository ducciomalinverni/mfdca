import sys
import Bio.SeqIO
import numpy as np
import scipy.sparse
from sklearn.preprocessing import OneHotEncoder
from scipy.spatial.distance import pdist,squareform

def binarizeMSA(fastaFile):
    """ Expands the categorical MSA in extended binary form.


    Keyword Arguments:
        fastaFile (str): The file containing the MSA in fasta format.

    Returns:
        binaryMSA (ndarry): A 2D scipy.sparse.csr.csr_matrix of dimensions (B,21*N), 
                            where B is the number of sequences in the MSA and N
                            the original width of the MSA.
    """
    
    msa=Bio.SeqIO.parse(fastaFile,'fasta')
        
    seqs=[list(str(s.seq)) for s in msa]     
    aaDict={'-':0, 'X':0,'Z':0,'B':0, 'A':1, 'C':2, 'D':3, 'E':4, 'F':5, 'G':6, 'H':7, 'I':8, 'K':9, 'L':10, 'M':11,
            'N':12, 'P':13, 'Q':14, 'R':15, 'S':16, 'T':17, 'V':18, 'W':19, 'Y':20}   

    msa= np.asarray([[aaDict[aa] for aa in seq] for seq in seqs])
    enc=OneHotEncoder(categories=[range(21) for n in range(msa.shape[1])])
    binaryMSA=enc.fit_transform(msa)

    return binaryMSA,msa

def mfDCA(msaFile,wid,fullOutput,outPrefix):
    if msaFile==None:
        print("No MSA provided")
        sys.exit(1)
    
    # Load data
    bmsa,msa = binarizeMSA(msaFile)
    N = msa.shape[1]
    q = 21
    pc = 0.5
    
    # Compue sequence weights
    if(wid<1):
        print("Computing weights at " + str(wid*100) +"% identity.")
        d = 1-squareform(pdist(msa,metric='hamming'))
        ws= 1./((d>wid).sum(axis=0))
        Beff = ws.sum()
    else:
        Beff = msa.shape[0]
        ws = np.ones(Beff)

    print("N= "+str(N)+" , B= "+str(msa.shape[0])+" , Beff= "+str(Beff)+" , wid= "+str(wid)+" , pc= " + str(pc)+" , q= "+str(q))
                                        
    # Compute weighted and regularized frequencies and co-frequencies
    print("Building connected correlations")
    fi = scipy.sparse.diags(ws).dot(bmsa).sum(axis=0)/Beff
    fi = (1-pc)*fi + pc/float(q)
    fij = bmsa.T.dot(scipy.sparse.diags(ws)).dot(bmsa).todense()/Beff
    fij = (1-pc)*fij + pc/float(q*q)
    fij -= np.kron(np.eye(N),np.ones((q,q)))*pc/float(q*q) - np.eye(N*q)*pc/float(q)
    
    Cred = fij-np.outer(fi,fi)
    Cred = np.delete(Cred,np.arange(0,N*q,q),axis=0)
    Cred = np.delete(Cred,np.arange(0,N*q,q),axis=1)

    # Compute Couplings in MF gauge
    print("Computing model couplings")
    J = -np.linalg.inv(Cred)
    
    # Shift parameters to Ising gauge
    J_full=np.insert(J,range(0,N*(q-1),q-1),0.,axis=0)
    J_full=np.insert(J_full,range(0,N*(q-1),q-1),0.,axis=1)
    for i in range(N):
        for j in range(N):
            block=J_full[(i*q):(i*q)+q,(j*q):(j*q)+q]
            J_full[(i*q):(i*q)+q,(j*q):(j*q)+q] = block - block.mean(axis=0) - block.mean(axis=1) + block.mean()

    # Conpute Frobenius norm scores
    print("Computing DCA scores")
    S = np.zeros((N,N))
    for i in range(N):
        for j in range(i+1,N):
            S[i,j] = np.linalg.norm(J_full[(i*q):(i+1)*q,(j*q):(j+1)*q],'fro')
            S[j,i] = S[i,j]

    # Apply APC Transform
    S_apc = S - np.outer(S.mean(axis=1),S.mean(axis=0))/S.mean()
    S_apc = np.triu(S_apc) + np.triu(S_apc).T -2*np.diag(np.diag(S_apc))
    s=scipy.spatial.distance.squareform(S_apc)
    
    # Output only upper triangular part without diagonal
    np.savetxt(outPrefix+'_dca.dat',s,fmt='%f',delimiter=' ')

    # If full parameters output required, compute fields and save prm file
    if(fullOutput):
        print("Computing full model parameters")
        # Compute fields
        h = np.zeros(N*(q-1))
        for i in range(N):
            for A in range(0,q-1):
                h[i*(q-1)+A] = np.log(fi[0,i*q+A+1]/fi[0,i*q])
                for j in range(N):
                    if j != i:
                        for B in range(0,q-1):
                            h[i*(q-1)+A]-= J[i*(q-1)+A,j*(q-1)+B]*fi[0,j*q+B+1]

        # Shift fields to ising gauge
        h_full=np.insert(h,range(0,N*(q-1),q-1),0.,axis=0)
        for i in range(N):
            for j in range(N):
                block=J_full[(i*q):(i*q)+q,(j*q):(j*q)+q]
                if i!=j:
                    h_full[q*i:q*i+q]+=2*np.squeeze(np.asarray(block.mean(axis=1)))

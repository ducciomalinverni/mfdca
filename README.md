# mfDCA
**mfDCA** is an open-source python implementation of the mean-field Direct-Coupling Analysis algorithm.

Relevant references describing the inference and scoring scheme are.

	 * Morcos et al., 2011, PNAS, _Direct-coupling analysis of residue coevolution captures native contacts across many protein families._.

	 * Ekeberg et al., 2013, Phys Rev E, _Improved contact prediction in proteins: Using pseudolikelihoods to infer Potts models_

# Requirements
mfDCA uses relies on the Biopython and sklearn packages.

# Usage
```shell
 ./mfdca [-h] [-wid WID] [-full] [-o O] -f F

 optional arguments:
 	   -h, --help  show this help message and exit
	   -f F        MSA file in fasta format
	   -wid WID    Max. Identity reweighting in (0,1) [default 0.9]
	   -o O        Output prefix [default output]
	   -full       (Currently unsupported) Use to output the full couplings and fields parameters
```

# Output format
The DCA scores are output after APC correction (see Eckeberg2013). As scores are symmetric, only the upper diagonal is stored.
The output file is asingle column ASCII file, containing the DCA scores in the following order

```shell
S(1,2)
S(1,3)
...
S(1,N-1)
S(1,N)
S(2,3)
...
S(2,N)
...
S(N-1,1)
...
S(N-1,N)
```